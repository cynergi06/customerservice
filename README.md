# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version: 1.0.0.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
	1. Goto the mapped directory/folder then run "CustomerService.sln" using Visual Studio 2017 in Administrator mode.
* Dependencies
	1. Microsoft Visual Studio 2017
* Database configuration
* How to run tests
	1. Running the azure function locally
		1. Once set-up has been completed, right click "Solution" then click "Clean solution".
		2. Right click "Solution" then click "Build" or "Rebuild" solution.
		3. Since we're ready to run the application, press F5
		4. You may now test the available azure functions url.
	2. Running the online azure functions
		1. Paste the following url's in the browser then provide your name
			* http://cynergi06-customerservice.azurewebsites.net/api/GetListOfCustomers?name=yourname
			* http://cynergi06-customerservice.azurewebsites.net/api/GetTotalBetsPerCustomer?name=yourname
			* http://cynergi06-customerservice.azurewebsites.net/api/GetTotalAmountPerCustomer?name=yourname
			* http://cynergi06-customerservice.azurewebsites.net/api/GetTotalAmountAllCustomers?name=yourname
			* http://cynergi06-customerservice.azurewebsites.net/api/GetAtRiskCustomers?name=yourname
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact