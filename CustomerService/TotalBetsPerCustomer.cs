﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerService
{
    public class TotalBetsPerCustomer
    {
        public int CustomerKey { get; set; }

        public string CustomerName { get; set; }

        public int TotalBets { get; set; }
    }
}
