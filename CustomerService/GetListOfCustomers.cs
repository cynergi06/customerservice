using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;

namespace CustomerService
{
    public static class CustomerFunctions
    {
        [FunctionName("GetListOfCustomers")]
        public static async Task<HttpResponseMessage> GetListOfCustomers([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");

            // parse query parameter
            string name = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "name", true) == 0)
                .Value;

            // Get request body
            dynamic data = await req.Content.ReadAsAsync<object>();

            // Set name to query string or body data
            name = name ?? data?.name;

            List<Customer> customers = CustomerDataContext.GetCustomers();

            return name == null
                ? req.CreateResponse(HttpStatusCode.BadRequest, "Please pass a name on the query string or in the request body")
                : req.CreateResponse(HttpStatusCode.OK, customers);
        }

        [FunctionName("GetTotalBetsPerCustomer")]
        public static async Task<HttpResponseMessage> GetTotalBetsPerCustomer([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");

            // parse query parameter
            string name = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "name", true) == 0)
                .Value;

            // Get request body
            dynamic data = await req.Content.ReadAsAsync<object>();

            // Set name to query string or body data
            name = name ?? data?.name;

            List<CustomerBet> customerBets = CustomerDataContext.GetCustomerBets();
            var groupCustomerBets = customerBets.GroupBy(ck => ck.CustomerKey, (key, count) => new { CustomerKey = key, TotalCount = count.Count() }).ToList();

            List<Customer> customers = CustomerDataContext.GetCustomers();

            var totalBetsPerCustomerJn = (from g in groupCustomerBets
                                          join c in customers
                                          on new { CustomerKey = g.CustomerKey } equals new { CustomerKey = c.CustomerKey }
                                          select new
                                          {
                                                CustomerKey = c.CustomerKey
                                              , CustomerName = c.CustomerName
                                              , TotalBets = g.TotalCount
                                          }).ToList();

            List<TotalBetsPerCustomer> totalBetsPerCustomer = new List<TotalBetsPerCustomer>();

            for (int i = 0; i <= totalBetsPerCustomerJn.Count - 1; i++)
            {
                totalBetsPerCustomer.Add(
                    new TotalBetsPerCustomer()
                    {
                          CustomerKey = totalBetsPerCustomerJn[i].CustomerKey
                        , CustomerName = totalBetsPerCustomerJn[i].CustomerName
                        , TotalBets = totalBetsPerCustomerJn[i].TotalBets
                    }
                );
            };

            return name == null
                ? req.CreateResponse(HttpStatusCode.BadRequest, "Please pass a name on the query string or in the request body")
                : req.CreateResponse(HttpStatusCode.OK, totalBetsPerCustomer);
        }

        [FunctionName("GetTotalAmountPerCustomer")]
        public static async Task<HttpResponseMessage> GetTotalAmountPerCustomer([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");

            // parse query parameter
            string name = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "name", true) == 0)
                .Value;

            // Get request body
            dynamic data = await req.Content.ReadAsAsync<object>();

            // Set name to query string or body data
            name = name ?? data?.name;

            List<CustomerBet> customerBets = CustomerDataContext.GetCustomerBets();
            var groupCustomerAmount = customerBets.GroupBy(ck => ck.CustomerKey).Select(g => new { CustomerKey = g.Key, TotalAmount = g.Sum(s => s.Amount) });

            List<Customer> customers = CustomerDataContext.GetCustomers();

            var totalAmountPerCustomerJn = (from g in groupCustomerAmount
                                          join c in customers
                                          on new { CustomerKey = g.CustomerKey } equals new { CustomerKey = c.CustomerKey }
                                          select new
                                          {
                                                CustomerKey = c.CustomerKey
                                              , CustomerName = c.CustomerName
                                              , TotalAmount = g.TotalAmount
                                          }).ToList();

            List<TotalAmountPerCustomer> totalAmountPerCustomer = new List<TotalAmountPerCustomer>();

            for (int i = 0; i <= totalAmountPerCustomerJn.Count - 1; i++)
            {
                totalAmountPerCustomer.Add(
                    new TotalAmountPerCustomer()
                    {
                          CustomerKey = totalAmountPerCustomerJn[i].CustomerKey
                        , CustomerName = totalAmountPerCustomerJn[i].CustomerName
                        , TotalAmount = totalAmountPerCustomerJn[i].TotalAmount
                    }
                );
            };

            return name == null
                ? req.CreateResponse(HttpStatusCode.BadRequest, "Please pass a name on the query string or in the request body")
                : req.CreateResponse(HttpStatusCode.OK, totalAmountPerCustomer);
        }

        [FunctionName("GetTotalAmountAllCustomers")]
        public static async Task<HttpResponseMessage> GetTotalAmountAllCustomers([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");

            // parse query parameter
            string name = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "name", true) == 0)
                .Value;

            // Get request body
            dynamic data = await req.Content.ReadAsAsync<object>();

            // Set name to query string or body data
            name = name ?? data?.name;

            List<CustomerBet> customerBets = CustomerDataContext.GetCustomerBets();
            var totalAmountAllCustomers = customerBets.Sum(s => s.Amount);

            return name == null
                ? req.CreateResponse(HttpStatusCode.BadRequest, "Please pass a name on the query string or in the request body")
                : req.CreateResponse(HttpStatusCode.OK, totalAmountAllCustomers);
        }

        [FunctionName("GetAtRiskCustomers")]
        public static async Task<HttpResponseMessage> GetAtRiskCustomers([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");

            // parse query parameter
            string name = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "name", true) == 0)
                .Value;

            // Get request body
            dynamic data = await req.Content.ReadAsAsync<object>();

            // Set name to query string or body data
            name = name ?? data?.name;

            List<CustomerBet> customerBets = CustomerDataContext.GetCustomerBets();
            var atRiskCustomersAmount = customerBets.Where(c => c.Amount > 200).ToList();

            List<Customer> customer = CustomerDataContext.GetCustomers();
            var atRiskCustomersAmountJn = (from cb in atRiskCustomersAmount
                                           join c in customer
                                           on new { CustomerKey = cb.CustomerKey } equals new { CustomerKey = c.CustomerKey }
                                           select new
                                           {
                                                 CustomerKey = c.CustomerKey
                                               , CustomerName = c.CustomerName
                                               , HorseKey = cb.HorseKey
                                               , Amount = cb.Amount
                                           }).ToList();

            List<AtRiskCustomer> atRiskCustomers = new List<AtRiskCustomer>();

            for (int i = 0; i <= atRiskCustomersAmountJn.Count - 1; i++)
            {
                atRiskCustomers.Add(
                    new AtRiskCustomer()
                    {
                          CustomerKey = atRiskCustomersAmountJn[i].CustomerKey
                        , CustomerName = atRiskCustomersAmountJn[i].CustomerName
                        , HorseKey = atRiskCustomersAmountJn[i].HorseKey
                        , Amount = atRiskCustomersAmountJn[i].Amount
                    }
                );
            };

            return name == null
                ? req.CreateResponse(HttpStatusCode.BadRequest, "Please pass a name on the query string or in the request body")
                : req.CreateResponse(HttpStatusCode.OK, atRiskCustomers);
        }
    }
}
