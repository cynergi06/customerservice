﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerService
{
    public class CustomerBet
    {
        public int CustomerKey { get; set; }

        public int HorseKey { get; set; }

        public decimal Amount { get; set; }
    }
}
