﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerService
{
    public class AtRiskCustomer
    {
        public int CustomerKey { get; set; }

        public string CustomerName { get; set; }

        public int HorseKey { get; set; }

        public decimal Amount { get; set; }
    }
}
