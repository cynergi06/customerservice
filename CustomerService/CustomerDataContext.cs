﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerService
{
    public static class CustomerDataContext
    {
        public static List<Customer> GetCustomers()
        {
            return new List<Customer>()
            {
                  new Customer(){ CustomerKey = 1, CustomerName = "Charles Yu" }
                , new Customer(){ CustomerKey = 2, CustomerName = "Giddel Yu" }
                , new Customer(){ CustomerKey = 3, CustomerName = "David Yu" }
                , new Customer(){ CustomerKey = 4, CustomerName = "Nicka Yu" }
            };
        }

        public static List<CustomerBet> GetCustomerBets()
        {
            return new List<CustomerBet>()
            {
                  new CustomerBet(){ CustomerKey = 1, HorseKey = 1, Amount = 50 }
                , new CustomerBet(){ CustomerKey = 1, HorseKey = 2, Amount = 200 }

                , new CustomerBet(){ CustomerKey = 2, HorseKey = 2, Amount = 100 }

                , new CustomerBet(){ CustomerKey = 3, HorseKey = 2, Amount = 250 }
                , new CustomerBet(){ CustomerKey = 3, HorseKey = 3, Amount = 150 }
                , new CustomerBet(){ CustomerKey = 3, HorseKey = 4, Amount = 350 }
                , new CustomerBet(){ CustomerKey = 3, HorseKey = 5, Amount = 350 }
                , new CustomerBet(){ CustomerKey = 3, HorseKey = 6, Amount = 350 }

                , new CustomerBet(){ CustomerKey = 4, HorseKey = 6, Amount = 300 }
                , new CustomerBet(){ CustomerKey = 4, HorseKey = 7, Amount = 450 }
                , new CustomerBet(){ CustomerKey = 4, HorseKey = 8, Amount = 350 }
            };
        }
    }
}
