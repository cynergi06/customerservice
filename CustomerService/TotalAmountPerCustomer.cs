﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerService
{
    public class TotalAmountPerCustomer
    {
        public int CustomerKey { get; set; }

        public string CustomerName { get; set; }

        public decimal TotalAmount { get; set; }
    }
}
